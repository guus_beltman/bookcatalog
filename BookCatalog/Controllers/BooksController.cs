﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BookCatalog.Models;

namespace BookCatalog.Controllers
{
    //http://www.asp.net/web-api/overview/creating-web-apis/creating-a-web-api-that-supports-crud-operations
    public class BooksController : ApiController
    {
        
        private IBookRepository _repository;
        
        public BooksController(IBookRepository repository)  
        {
            _repository = repository;
        }


        /// <summary>
        /// /api/books
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Book> GetAllProducts()
        {
            return _repository.GetAll();
        }

        /// <summary>
        /// /api/books/id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Book GetProduct(int id)
        {
            Book item = _repository.Get(id);
            if (item == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
            return item;
        }

        /// <summary>
        /// /api/books?category=category
        /// </summary>
        /// <param name="category"></param>
        /// <returns></returns>
        public IEnumerable<Book> GetProductsByCategory(string category)
        {
            return _repository.GetAll().Where(
                p => string.Equals(p.Category, category, StringComparison.OrdinalIgnoreCase));
        }

        public HttpResponseMessage PostProduct(Book item)
        {
            //TODO: model validation

            item = _repository.Add(item);
            var response = Request.CreateResponse<Book>(HttpStatusCode.Created, item);

            string uri = Url.Link("DefaultApi", new { id = item.Id });
            response.Headers.Location = new Uri(uri);
            return response;
        }

        public void PutProduct(int id, Book book)
        {
            book.Id = id;
            if (!_repository.Update(book))
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }

        public void DeleteProduct(int id)
        {
            Book item = _repository.Get(id);
            if (item == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }

            _repository.Remove(id);
        }

    }
}
