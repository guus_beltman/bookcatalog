﻿using System;
using System.Collections.Generic;

namespace BookCatalog.Models
{
    public class BookRepository : IBookRepository
    {
        private List<Book> products = new List<Book>();
        private int _nextId = 1;

        public BookRepository()
        {
            Add(new Book { Name = "Tomato soup", Category = "Groceries", Price = 1.39M });
            Add(new Book { Name = "Yo-yo", Category = "Toys", Price = 3.75M });
            Add(new Book { Name = "Hammer", Category = "Hardware", Price = 16.99M });
        }

        public IEnumerable<Book> GetAll()
        {
            return products;
        }

        public Book Get(int id)
        {
            return products.Find(p => p.Id == id);
        }

        public Book Add(Book item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }
            item.Id = _nextId++;
            products.Add(item);
            return item;
        }

        public void Remove(int id)
        {
            products.RemoveAll(p => p.Id == id);
        }

        public bool Update(Book item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }
            int index = products.FindIndex(p => p.Id == item.Id);
            if (index == -1)
            {
                return false;
            }
            products.RemoveAt(index);
            products.Add(item);
            return true;
        }
    }
}